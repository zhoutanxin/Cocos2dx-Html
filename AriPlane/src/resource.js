var s_HelloWorld = "HelloWorld.jpg";
var s_CloseNormal = "CloseNormal.png";
var s_CloseSelected = "CloseSelected.png";
var s_BoyHero = "BoyHero.png";
// 将资源写在这里，程序会预加载这些资源
var s_Plane = "plane.png";

var g_resources = [
    //image
    {src:s_HelloWorld},
    {src:s_CloseNormal},
    {src:s_CloseSelected},
    {src:s_Plane},
    {src:s_BoyHero}
    //plist

    //fnt

    //tmx

    //bgm

    //effect
];